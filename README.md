# Suru
Suru is a lean GUI application to work with [todo.txt](http://www.todotxt.com) files.

Suru is build using native C++11 and Qt6.

# License
Suru is Copyright (C) 2016-2025 Bart Clephas, and is licensed under GNU GPL (v3) license,
the current version is available in LICENSE file.

Todo.txt, Copyright (C) 2006-2016 Gina Trapani  
Suru icon, Copyright (C) 2015-2016 [BlackVariant](http://blackvariant.deviantart.com)

