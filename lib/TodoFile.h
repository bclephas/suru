/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_TODOFILE
#define TODOTXT_TODOFILE

#include "TodoTaskCollection.h"

#include <memory>
#include <string>
#include <vector>

class TodoTask;

typedef std::shared_ptr<TodoTask> TodoTask_ptr;

/* Represents a todo.txt list.
 *
 * Open tasks are stored to todo.txt
 * Completed tasks are stored to done.txt
 */
class TodoFile
{
public:
    static const bool WRITE_ONLY_INCOMPLETE_TASKS = false;
    static const bool WRITE_ONLY_COMPLETE_TASKS   = true;

public:
    TodoFile();
    virtual ~TodoFile();

    bool load(std::string path);
    bool save(std::string path);

    std::vector<TodoTask_ptr> tasks();
    void setTasks(std::vector<TodoTask_ptr> tasks);

protected:
    /* Load tasks from istream. User needs to ensure a valid stream
     * and close it */
    virtual bool load(std::istream &stream);

    virtual void readTasks(std::vector<std::string> &lines);
    virtual bool writeTasks(std::ostream &stream, bool writeCompleteTasks);

private:
    TodoTaskCollection taskcollection;
};

#endif /* TODOTXT_TODOFILE */

