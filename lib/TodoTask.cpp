/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include <algorithm>
#include <ctime>
#include <forward_list>
#include <iterator>
#include <iomanip>
#include <regex>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>

#include "TodoTask.h"

/* Have a portable way to parse date, analogue to strptime.
   See http://stackoverflow.com/a/33542189 for the original implementation */
extern "C" char *parseDateToString(const char *s, const char *f, struct tm *tm)
{
    std::istringstream input(s);
    input.imbue(std::locale(setlocale(LC_ALL, nullptr)));
    /* std::get_time format specifiers are equal to strptime */
    input >> std::get_time(tm, f);
    if (input.fail()) {
        return nullptr;
    }
    return (char *)(s + input.tellg());
}

std::string join(const std::forward_list<std::string>& vec, const char* delim)
{
    std::stringstream result;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<std::string>(result, delim));
    return result.str();
}

std::ostream& operator<<(std::ostream &stream, const TodoTask &task)
{
    std::string contexts;
    for (auto const& s : task.contexts) { contexts += " "; contexts += s; }
    std::string projects;
    for (auto const& s : task.projects) { projects += " "; projects += s; }

    /* transform date to string */
    char buf[256];
    strftime(buf, sizeof(buf), "%Y-%m-%d", &task.creation_date);
    std::string creation_date = buf;
    strftime(buf, sizeof(buf), "%Y-%m-%d", &task.completion_date);
    std::string completion_date = buf;

    return stream << "TodoTask("
                  << task.complete
                  << ": " << completion_date
                  << ": " << creation_date
                  << ": " << task.priority
                  << ": " << task.description
                  << ": " << contexts
                  << ": " << projects
                  << ")";
}

/* Trim implementation: http://www.cplusplus.com/faq/sequences/strings/trim */
std::string& trim_right(std::string &s,
                        const std::string& delimiters = " \f\n\r\t\v")
{
    return s.erase(s.find_last_not_of(delimiters) + 1);
}

std::string& trim_left(std::string &s,
                       const std::string& delimiters = " \f\n\r\t\v")
{
    return s.erase(0, s.find_first_not_of(delimiters));
}

std::string& trim(std::string &s,
                  const std::string& delimiters = " \f\n\r\t\v")
{
    return trim_left(trim_right(s, delimiters), delimiters);
}

TodoTask::TodoTask(std::string line)
    : priority('\0')
    , complete(false)
    , contexts()
    , projects()
    , description("")
    , raw_line("")
{
    memset(&creation_date, 0, sizeof(creation_date));
    memset(&completion_date, 0, sizeof(completion_date));

    parseLine(line);
}

TodoTask::~TodoTask()
{
}

void TodoTask::parseLine(std::string line)
{
    raw_line = trim(line);

    /* tokenize input */
    std::istringstream iss(line);
    std::forward_list<std::string> tokens { std::istream_iterator<std::string>{iss},
                                            std::istream_iterator<std::string>{} };

    /* ignore lines with no task data */
    if (tokens.empty()) {
        return;
    }

    /* check for completion */
    if (tokens.front() == "x") {
        complete = true;
        tokens.pop_front();

        /* check if completion date is present */
        bool valid_date = parseDate(tokens.front(), &completion_date);
        if (valid_date) {
            tokens.pop_front();
        }
    }

    /* check for priority */
    if (std::regex_match(tokens.front(), std::regex("^\\([A-Z]\\)$"))) {
        priority = tokens.front()[1];
        tokens.pop_front();
    }

    /* check for optional creation date */
    bool valid_date = parseDate(tokens.front(), &creation_date);
    if (valid_date) {
        tokens.pop_front();
    }

    /* fill description (contains all remaining tokens) */
    const char *delim = " ";
    description = join(tokens, delim);
    description = trim(description);

    /* parse contexts and projects */
    for (std::forward_list<std::string>::const_iterator it = tokens.begin(); it != tokens.end(); ++it) {
        std::string word = *it;

        /* Ignore tokens of size 1 for context and project */
        if (word.length() <= 1) {
            continue;
        }

        if (word[0] == '@') {
            contexts.push_back(word.substr(1));
        } else if (word[0] == '+') {
            projects.push_back(word.substr(1));
        }
    }
}

bool TodoTask::parseDate(std::string date, struct tm *tm)
{
    char *result = parseDateToString(date.c_str(), "%Y-%m-%d", tm);
    if (result == NULL) {
        memset(tm, 0, sizeof(*tm));
    }
    return (result != NULL);
}

