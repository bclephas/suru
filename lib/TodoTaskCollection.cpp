/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "TodoTaskCollection.h"

#include "TodoTask.h"

#include <algorithm>

template<typename T>
void removeDuplicates(std::vector<T> &vec)
{
    std::sort(vec.begin(), vec.end());
    vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}

TodoTaskCollection::TodoTaskCollection()
{
}

TodoTaskCollection::~TodoTaskCollection()
{
}

void TodoTaskCollection::clear()
{
    items.clear();
}

std::vector<std::string> TodoTaskCollection::getProjects()
{
    std::vector<std::string> projects;

    for (TodoTask_ptr task : items) {
        copy(task->projects.begin(), task->projects.end(), back_inserter(projects));
    }
    removeDuplicates(projects);

    return projects;
}

std::vector<std::string> TodoTaskCollection::getContexts()
{
    std::vector<std::string> contexts;

    for (TodoTask_ptr task : items) {
        copy(task->contexts.begin(), task->contexts.end(), back_inserter(contexts));
    }
    removeDuplicates(contexts);

    return contexts;
}

