/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_TODOTASK
#define TODOTXT_TODOTASK

#include <ctime>
#include <string>
#include <sstream>
#include <vector>

/* Represents a task in todo.txt format
 *
 * Spec: https://github.com/ginatrapani/todo.txt-cli/wiki/The-Todo.txt-Format
 *
 * Task Context: @context, e.g. @phone
 * Project: +project, e.g. +home
 * Priority: (priority), e.g. (A) or (B); A has higher priority than B. Priority goes to Z
 * Lines starting with x are complete/done
 *
 * Incomplete tasks:
 * Priority always appears first, like "(A) Buy bread"
 * Optional task creation date after priority and a space in YYYY-MM-DD format, like "(A) 2016-07-15 Buy bread"
 * Context and project may appear anywhere after the priority and optional date
 *
 * Complete tasks:
 * 'x' followed by a space always appears first, like "x Buy bread"
 * Optional completion date after the x in YYYY-MM-DD format, like "x 2016-07-15 2016-07-15 Buy bread"
 * Priority is optionally discarded
 */
class TodoTask
{
public:
    TodoTask(std::string line);
    ~TodoTask();

private:
    /* Parse a string in todo.txt format */
    void parseLine(std::string line);

    /* Parse a word to a text. Return false if not a date, tm is zeroed */
    bool parseDate(std::string word, struct tm *tm);

public:
    char priority;
    bool complete;
    std::vector<std::string> contexts;
    std::vector<std::string> projects;
    struct tm creation_date;
    struct tm completion_date;
    std::string description;
    std::string raw_line;
};

std::ostream& operator<<(std::ostream &stream, const TodoTask &task);

#endif /* TODOTXT_TODOTASK */

