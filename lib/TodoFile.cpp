/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include <fstream>
#include <string>
#include <sstream>

#include "TodoFile.h"
#include "TodoTask.h"

TodoFile::TodoFile()
{
}

TodoFile::~TodoFile()
{
}

bool TodoFile::load(std::string path)
{
    bool result = false;

    std::ifstream file(path + "/todo.txt");
    if (file.good()) {
        result = load(file);
    }
    file.close();

    return result;
}

bool TodoFile::load(std::istream &stream)
{
    std::string line;
    std::vector<std::string> lines;

    taskcollection.clear();
    while (std::getline(stream, line)) {
        lines.push_back(line);
    }
    readTasks(lines);

    // fail is also set on EOF, so on EOF we are actually good
    bool errorOnRead = (stream.fail() && !stream.eof());
    return !errorOnRead;
}

bool TodoFile::save(std::string path)
{
    bool todoResult = false;
    bool doneResult = false;

    std::ofstream todoFile(path + "/todo.txt", std::ofstream::out);
    if (todoFile.good()) {
        todoResult = writeTasks(todoFile, TodoFile::WRITE_ONLY_INCOMPLETE_TASKS);
    }
    todoFile.close();

    std::ofstream doneFile(path + "/done.txt", std::ofstream::out | std::ofstream::app);
    if (doneFile.good()) {
        doneResult = writeTasks(doneFile, TodoFile::WRITE_ONLY_COMPLETE_TASKS);
    }
    doneFile.close();

    return todoResult && doneResult;
}

std::vector<TodoTask_ptr> TodoFile::tasks()
{
    return taskcollection.items;
}

void TodoFile::setTasks(std::vector<TodoTask_ptr> tasks)
{
    taskcollection.items = tasks;
}

void TodoFile::readTasks(std::vector<std::string> &lines)
{
    for (std::string line : lines) {
        std::shared_ptr<TodoTask> newTask(new TodoTask(line));
        if (!newTask->raw_line.empty()) {
            taskcollection.items.push_back(newTask);
        }
    }
}

bool TodoFile::writeTasks(std::ostream &stream, bool writeCompleteTasks)
{
    for (TodoTask_ptr task : taskcollection.items) {
        if (writeCompleteTasks == task->complete) {
            stream.write(task->raw_line.c_str(), task->raw_line.length());
            stream.write("\n", 1);
        }
    }
    return !stream.fail();
}

