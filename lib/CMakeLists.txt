cmake_minimum_required(VERSION 3.31)
project(todotxt)

set(CMAKE_CXX_STANDARD 11)

set(SRCS TodoFile.cpp
         TodoTask.cpp
         TodoTaskCollection.cpp)
add_library(todotxt SHARED ${SRCS})

