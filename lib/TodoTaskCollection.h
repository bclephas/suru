/*
    libtodotxt - small library with work with todo.txt files
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_TODOTASKCOLLECTION
#define TODOTXT_TODOTASKCOLLECTION

#include <memory>
#include <string>
#include <vector>

class TodoTask;

typedef std::shared_ptr<TodoTask> TodoTask_ptr;

class TodoTaskCollection
{
public:
    TodoTaskCollection();
    virtual ~TodoTaskCollection();

    void clear();

    /* Get all projects from all tasks */
    std::vector<std::string> getProjects();

    /* Get all contexts from all tasks */
    std::vector<std::string> getContexts();

public:
    std::vector<TodoTask_ptr> items;
};

#endif /* TODOTXT_TODOTASKCOLLECTION */

