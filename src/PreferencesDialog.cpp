/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "PreferencesDialog.h"

#include <QtWidgets>

const QString PreferencesDialog::SHOW_PROJECTS_PANEL_SETTINGS_STRING = "ShowProjectsPanel";
const QString PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING = "TodoFilesFolder";

PreferencesDialog::PreferencesDialog(QWidget *parent)
    : QDialog(parent)
    , labelTodoFilesFolder(new QLabel)
    , lineEditTodoFilesFolder(new QLineEdit)
    , labelShowProjectsPanel(new QLabel)
    , checkBoxShowProjectsPanel(new QCheckBox)
    , buttonBrowseTodoFilesFolder(new QPushButton)
    , buttonOk(new QPushButton)
    , buttonCancel(new QPushButton)
{
    setModal(true);
    setWindowTitle(tr("Preferences"));

    labelTodoFilesFolder->setText(tr("Location of todo.txt files"));
    lineEditTodoFilesFolder->setText("~");
    lineEditTodoFilesFolder->setObjectName(QString(TODO_FILES_FOLDER_SETTINGS_STRING));
    connect(lineEditTodoFilesFolder, SIGNAL(textChanged(const QString &)), SLOT(storeText(const QString &)));
    buttonBrowseTodoFilesFolder->setText("...");
    buttonBrowseTodoFilesFolder->setFixedWidth(30);
    connect(buttonBrowseTodoFilesFolder, SIGNAL(clicked()), this, SLOT(browse()));

    QHBoxLayout *locationLayout = new QHBoxLayout;
    locationLayout->addWidget(lineEditTodoFilesFolder);
    locationLayout->addWidget(buttonBrowseTodoFilesFolder);

    labelShowProjectsPanel->setText(tr("Show projects panel"));
    checkBoxShowProjectsPanel->setChecked(false);
    checkBoxShowProjectsPanel->setObjectName(QString("ShowProjectsPanel"));
    connect(checkBoxShowProjectsPanel, SIGNAL(toggled(bool)), SLOT(storeCheck(bool)));

    buttonOk->setText(tr("OK"));
    connect(buttonOk, SIGNAL(clicked()), this, SLOT(accept()));

    buttonCancel->setText(tr("Cancel"));
    connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));

    QHBoxLayout *dialogButtonsLayout = new QHBoxLayout;
    dialogButtonsLayout->addWidget(buttonCancel);
    dialogButtonsLayout->addWidget(buttonOk);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(labelTodoFilesFolder, 0, 0);
    layout->addLayout(locationLayout, 0, 1);
    layout->addWidget(labelShowProjectsPanel, 1, 0);
    layout->addWidget(checkBoxShowProjectsPanel, 1, 1);
    layout->addLayout(dialogButtonsLayout, 2, 1);

    setLayout(layout);
}

PreferencesDialog::~PreferencesDialog()
{
    delete buttonOk;
    delete buttonCancel;
    delete buttonBrowseTodoFilesFolder;
    delete checkBoxShowProjectsPanel;
    delete labelShowProjectsPanel;
    delete lineEditTodoFilesFolder;
    delete labelTodoFilesFolder;
    delete layout();
}

/* On opening of the dialog, restore the configuration */
void PreferencesDialog::showEvent(QShowEvent *event)
{
    restoreConfiguration();
}

void PreferencesDialog::restoreConfiguration()
{
    QSettings settings;
    foreach(const QString &key, settings.childKeys())
    {
        QLineEdit *le = findChild<QLineEdit*>(key);
        if (le) {
            le->setText(settings.value(key).toString());
        }
        QCheckBox *cb = findChild<QCheckBox*>(key);
        if (cb) {
            cb->setChecked(settings.value(key).toBool());
        }
    }
}

void PreferencesDialog::storeCheck(bool checked)
{
    QString key = sender()->objectName();

    QSettings settings;
    settings.setValue(key, checked);
}

void PreferencesDialog::storeText(const QString &text)
{
    QString key = sender()->objectName();

    QSettings settings;
    settings.setValue(key, text);
}

QString PreferencesDialog::todoTxtFolderLocation()
{
    return lineEditTodoFilesFolder->text();
}

bool PreferencesDialog::showProjectsPanel()
{
    return checkBoxShowProjectsPanel->checkState() == Qt::Checked;
}

void PreferencesDialog::browse()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                                                          tr("todo.txt location"),
                                                          lineEditTodoFilesFolder->text(),
                                                          QFileDialog::ShowDirsOnly);

    if (!directory.isEmpty()) {
        lineEditTodoFilesFolder->setText(directory);
    }
}

