/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "TaskModel.h"

#include "TodoFile.h"
#include "TodoTask.h"

#include <QBrush>
#include <QDebug>
#include <QFont>

#include <set>
#include <string>
#include <vector>

TaskModel::TaskAdapter::TaskAdapter()
    : task_id(-1)
    , task(nullptr)
{
}

TaskModel::TaskAdapter::TaskAdapter(qsizetype task_id, TodoTask_ptr task)
    : task_id(task_id)
    , task(task)
{
}

TaskModel::TaskAdapter::TaskAdapter(const TaskAdapter &other)
    : task_id(other.task_id)
    , task(other.task)
{
}

TaskModel::TaskAdapter::~TaskAdapter()
{
}

// ==================================

TaskModel::TaskModel(QObject *parent)
    : QAbstractItemModel(parent)
    , tasks(0)
{
}

TaskModel::~TaskModel()
{
}

void TaskModel::addTask(const QString taskDescription)
{
    beginInsertRows(QModelIndex(), tasks.size(), tasks.size());

    TodoTask_ptr newTask(new TodoTask(taskDescription.toStdString()));
    TaskModel::TaskAdapter task(tasks.size(), newTask);
    tasks.append(task);
    debugModel(tasks, "addTask");

    endInsertRows();

    notifyListeners();
}

bool TaskModel::loadFromDisk(QString basePath)
{
    bool result = false;

    beginResetModel();

    tasks.clear();
    debugModel(tasks, "loadFromDisk, clear");

    qDebug() << "Loading tasks from " + basePath + "/todo.txt";
    TodoFile todoFile;
    result = todoFile.load(basePath.toStdString());

    for (TodoTask_ptr task : todoFile.tasks()) {
        TaskModel::TaskAdapter newTask = { tasks.size(), task };
        tasks.append(newTask);
    }
    debugModel(tasks, "loadFromDisk");

    endResetModel();

    // Signal all data has changed
    emit dataChanged(QModelIndex(), QModelIndex());

    notifyListeners();

    return result;
}

bool TaskModel::saveToDisk(QString basePath)
{
    bool result = false;

    QVector<TodoTask_ptr> v;
    for (TaskModel::TaskAdapter ta : tasks) {
        v.append(ta.task);
    }
    qDebug() << "Saving" << v.size() << "tasks to" << basePath;
    TodoFile todoFile;
    std::vector<TodoTask_ptr> temp_v(v.begin(), v.end());
    todoFile.setTasks(temp_v);
    result = todoFile.save(basePath.toStdString());

    return result;
}

QModelIndex TaskModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (0 <= row && row < tasks.size()) {
        TodoTask_ptr task(tasks[row].task);
        return createIndex(row, column, task.get());
    }
    return QModelIndex();
}

QModelIndex TaskModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int TaskModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return (parent.isValid() && parent.column() != 0) ? 0 : tasks.size();
}

int TaskModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant TaskModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= tasks.size()) {
        qDebug() << "data() out of bounds: " << index.row() << "/" << tasks.size();
        return QVariant();
    }

    TodoTask_ptr task(tasks[index.row()].task);

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        QString raw_line = QString::fromStdString(task->raw_line);
        return QVariant(raw_line);
    }
    if (role == Qt::FontRole) {
        /* strikethrough completed tasks */
        if (task->complete) {
            QFont strikethroughFont;
            strikethroughFont.setStrikeOut(true);
            return strikethroughFont;
        }
        return QVariant();
    }
    if (role == Qt::ForegroundRole) {
        /* gray completed tasks */
        if (task->complete) {
            QBrush grayBrush(Qt::gray);
            return grayBrush;
        }
        return QVariant();
    }
    if (role == Qt::UserRole) {
        return QVariant::fromValue(tasks[index.row()]);
    }
    return QVariant();
}

bool TaskModel::hasChildren(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return false;
}

Qt::ItemFlags TaskModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled | QAbstractItemModel::flags(index);
}

bool TaskModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        if (index.row() >= tasks.size()) {
            qDebug() << "setData() out of bounds: " << index.row() << "/" << tasks.size();
            return false;
        }

        QString newTaskDescription = value.toString();

        TodoTask_ptr oldTask(tasks[index.row()].task);
        QString oldTaskDescription = QString::fromStdString(oldTask->raw_line);

        // Do not handle changed data if data didn't change
        if (newTaskDescription.compare(oldTaskDescription) == 0) {
            return false;
        }

        tasks[index.row()].task = TodoTask_ptr(new TodoTask(newTaskDescription.toStdString()));
        debugModel(tasks, "setData");

        // the transposedindex causes a qvector out of bounds assert on Window
        createIndex(index.column(), index.row());
        emit dataChanged(index, index);

        notifyListeners();

        return true;
    }
    return false;
}

QVector<QString> TaskModel::projects() const
{
    // Build list of projects in this model
    QVector<QString> projectList;
    std::vector<std::string> raw_list;
    for (TaskModel::TaskAdapter ta : tasks) {
        raw_list.insert(std::end(raw_list), std::begin(ta.task->projects), std::end(ta.task->projects));
        raw_list.insert(std::end(raw_list), std::begin(ta.task->contexts), std::end(ta.task->contexts));
    }

    std::set<std::string> uniqueProjects;
    for(unsigned i = 0; i < raw_list.size(); ++i) {
        uniqueProjects.insert(raw_list[i]);
    }

    for (std::string project : uniqueProjects) {
        projectList << QString::fromStdString(project);
    }

    return projectList;
}

void TaskModel::notifyListeners() const
{
    QVector<QString> projectList = projects();
    emit projectsChanged(projectList);
}

void TaskModel::debugModel(const QVector<TaskModel::TaskAdapter> &model, const QString context)
{
    qDebug() << "debugModel: " << context;
    for (TaskModel::TaskAdapter ta : model) {
        qDebug() << QString::fromStdString(ta.task->raw_line);
    }
}

