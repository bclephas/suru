/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include "ProjectModel.h"
#include "TodoTask.h"
#include <QDebug>

ProjectModel::ProjectModel(QVector<QString> projects, QObject *parent)
    : QAbstractItemModel(parent)
    , projects(projects)
{
}

ProjectModel::~ProjectModel()
{
}

QModelIndex ProjectModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (0 <= row && row < projects.size()) {
        return createIndex(row, column, nullptr);
    }
    return QModelIndex();
}

QModelIndex ProjectModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int ProjectModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return (parent.isValid() && parent.column() != 0) ? 0 : projects.size();
}

int ProjectModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant ProjectModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= projects.size()) {
        qDebug() << "data() out of bounds: " << index.row() << "/" << projects.size();
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        QString project = projects[index.row()];
        return QVariant(project);
    }
    return QVariant();
}

bool ProjectModel::hasChildren(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return false;
}

Qt::ItemFlags ProjectModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | QAbstractItemModel::flags(index);
}

void ProjectModel::setProjects(QVector<QString> projects)
{
    beginResetModel();

    this->projects = projects;
    this->projects.insert(0, "Clear");

    endResetModel();

    // Signal all data has changed
    emit dataChanged(QModelIndex(), QModelIndex());

    qDebug() << "New projects data: " << projects;
}

