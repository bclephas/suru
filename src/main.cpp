/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include <QApplication>

#include "TodoTxtMainWindow.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(TodoTxtApplication);

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("suru");
    QCoreApplication::setApplicationName("Suru");
    QCoreApplication::setApplicationVersion("0.1b");

    QIcon appIcon(":/resources/ToDo.ico");
    QApplication::setWindowIcon(appIcon);

    MainWindow mainWindow;
    mainWindow.show();

    return app.exec();
}

