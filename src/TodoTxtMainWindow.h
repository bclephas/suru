/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_UI_MAIN_WINDOW
#define TODOTXT_UI_MAIN_WINDOW

#include <QMainWindow>
#include <QSettings>

class QAction;
class QCloseEvent;
class QLineEdit;
class QListView;
class QPushButton;
class QSortFilterProxyModel;
class QVBoxLayout;

class PreferencesDialog;
class TaskModel;
class ProjectModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    virtual ~MainWindow();

    void closeEvent(QCloseEvent *event);

public slots:
    void showProjects(const QModelIndex &index);
    void filterTasks(const QString &text);
    void enterTask(const QString &text);
    void newTaskEntered();
    void addButtonClicked(bool checked = false);

    void showHelp();
    void showAboutDialog();
    void showPreferencesDialog();
    void refreshTasks();

private:
    void addTask();
    void updateUI();

    static const bool READ_TASKS     = true;
    static const bool NO_READ_TASKS  = false;
    static const bool WRITE_TASKS    = true;
    static const bool NO_WRITE_TASKS = false;
    void refreshTasksFromFile(bool loadFromFile = READ_TASKS, bool saveToFile = WRITE_TASKS);

private:
    void createMenuBar();

    QLineEdit *lineEditFilter;
    QLineEdit *lineEditNewTask;
    QListView *listViewProjects;
    QListView *listViewTasks;
    QPushButton *buttonAddTask;
    QSortFilterProxyModel *proxyTaskModel;
    QVBoxLayout *layout;

    PreferencesDialog *preferencesDialog;
    TaskModel *taskModel;
    ProjectModel *projectModel;

    QSettings settings;
    QString previousFolder;
};

#endif /* TODOTXT_UI_MAIN_WINDOW */

