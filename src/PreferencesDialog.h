/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_UI_PREFERENCES_DIALOG
#define TODOTXT_UI_PREFERENCES_DIALOG

#include <QDialog>

class QCheckBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSettings;

class PreferencesDialog : public QDialog
{
    Q_OBJECT

public:
    static const QString SHOW_PROJECTS_PANEL_SETTINGS_STRING;
    static const QString TODO_FILES_FOLDER_SETTINGS_STRING;

public:
    PreferencesDialog(QWidget *parent = nullptr);
    virtual ~PreferencesDialog();

    QString todoTxtFolderLocation();
    bool showProjectsPanel();

protected:
    virtual void /*QWidget::*/showEvent(QShowEvent *event);

public slots:
    void browse();

private slots:
    void storeCheck(bool checked);
    void storeText(const QString &);

private:
    void restoreConfiguration();

private:
    QLabel *labelTodoFilesFolder;
    QLineEdit *lineEditTodoFilesFolder;
    QPushButton *buttonBrowseTodoFilesFolder;

    QLabel *labelShowProjectsPanel;
    QCheckBox *checkBoxShowProjectsPanel;

    QPushButton *buttonOk;
    QPushButton *buttonCancel;
};

#endif /* TODOTXT_UI_PREFERENCES_DIALOG */

