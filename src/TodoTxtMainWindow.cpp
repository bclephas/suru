/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "TodoTxtMainWindow.h"
#include "PreferencesDialog.h"
#include "ProjectModel.h"
#include "SortTaskProxyModel.h"
#include "TaskModel.h"

#include <QGuiApplication>
#include <QScreen>
#include <QtWidgets>

MainWindow::MainWindow()
    : QMainWindow()
    , lineEditFilter(new QLineEdit)
    , lineEditNewTask(new QLineEdit)
    , listViewProjects(new QListView)
    , listViewTasks(new QListView)
    , buttonAddTask(new QPushButton)
    , layout(nullptr)
    , preferencesDialog(new PreferencesDialog)
    , taskModel(new TaskModel)
    , projectModel(new ProjectModel)
    , settings(QCoreApplication::organizationName(), QCoreApplication::applicationName())
    , previousFolder("")
{
    QWidget *centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);
    layout = new QVBoxLayout(centralWidget);

    QHBoxLayout *tasksLayout = new QHBoxLayout();
    tasksLayout->addWidget(listViewProjects);
    tasksLayout->addWidget(listViewTasks);

    QHBoxLayout *taskManagementLayout = new QHBoxLayout();
    taskManagementLayout->addWidget(lineEditNewTask);
    taskManagementLayout->addWidget(buttonAddTask);

    layout->addLayout(tasksLayout);
    layout->addLayout(taskManagementLayout);

    lineEditFilter->setPlaceholderText(tr("Search"));
    lineEditFilter->setClearButtonEnabled(true);
    connect(lineEditFilter, SIGNAL(textChanged(const QString &)), this, SLOT(filterTasks(const QString &)));

    projectModel = new ProjectModel();
    QItemSelectionModel *projectSelections = new QItemSelectionModel(projectModel);

    listViewProjects->setModel(projectModel);
    listViewProjects->setSelectionModel(projectSelections);
    listViewProjects->setViewMode(QListView::ListMode);
    listViewProjects->setAlternatingRowColors(true);
    listViewProjects->setUniformItemSizes(true);
    listViewProjects->setMovement(QListView::Free);
    listViewProjects->setFixedWidth(250);
    listViewProjects->show();

    connect(listViewProjects, SIGNAL(clicked(const QModelIndex &)), this, SLOT(showProjects(const QModelIndex &)));

    proxyTaskModel = new SortTaskProxyModel(this);
    proxyTaskModel->setSourceModel(taskModel);
    proxyTaskModel->setDynamicSortFilter(true);
    proxyTaskModel->setSortRole(Qt::UserRole);
    proxyTaskModel->sort(0);

    QItemSelectionModel *taskSelections = new QItemSelectionModel(proxyTaskModel);
    listViewTasks->setModel(proxyTaskModel);
    listViewTasks->setSelectionModel(taskSelections);
    listViewTasks->setViewMode(QListView::ListMode);
    listViewTasks->setAlternatingRowColors(true);
    listViewTasks->setUniformItemSizes(true);
    listViewTasks->setMovement(QListView::Free);
    listViewTasks->show();

    lineEditNewTask->setPlaceholderText(tr("New task... (example: [x for done] [(priority)] [completion date] [creation date] description [+tag] [@context] [key:value])"));
    lineEditNewTask->setClearButtonEnabled(true);
    connect(lineEditNewTask, SIGNAL(textChanged(const QString &)), this, SLOT(enterTask(const QString &)));
    connect(lineEditNewTask, SIGNAL(returnPressed()), this, SLOT(newTaskEntered()));

    buttonAddTask->setText(tr("Add"));
    buttonAddTask->setEnabled(false); /* start disabled */
    connect(buttonAddTask, SIGNAL(clicked(bool)), this, SLOT(addButtonClicked(bool)));

    createMenuBar();

    // Connect the models
    connect(taskModel, SIGNAL(projectsChanged(QVector<QString>)), projectModel, SLOT(setProjects(QVector<QString>)));

    updateUI();

    resize(screen()->availableGeometry().size() * 0.4);
}

MainWindow::~MainWindow()
{
    delete preferencesDialog;
    delete taskModel;
    delete proxyTaskModel;
    delete buttonAddTask;
    delete listViewTasks;
    delete listViewProjects;
    delete lineEditNewTask;
    delete lineEditFilter;
    delete layout;
    delete centralWidget();
}

void MainWindow::createMenuBar()
{
    /* https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html#names */
    /* On Linux: see for example /usr/share/icons/<theme>/actions/32/<name> */

    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

    const QIcon preferencesIcon = QIcon::fromTheme("preferences-system", QIcon(":/resources/preferences-system.png"));
    fileMenu->addAction(preferencesIcon, tr("&Preferences"), this, &MainWindow::showPreferencesDialog);

    fileMenu->addSeparator();

    const QIcon refreshIcon = QIcon::fromTheme("view-refresh", QIcon(":/resources/view-refresh.png"));
    fileMenu->addAction(refreshIcon, tr("&Refresh tasks"), this, &MainWindow::refreshTasks);

    fileMenu->addSeparator();

    const QIcon exitIcon = QIcon::fromTheme("application-exit", QIcon(":/resources/application-exit.png"));
    QAction *exitAction = fileMenu->addAction(exitIcon, tr("E&xit"), this, &QWidget::close);
    exitAction->setShortcuts(QKeySequence::Quit);

    // Add a in-line line edit to allow filtering
    QWidget* w = new QWidget();
    QHBoxLayout* layout = new QHBoxLayout(w);
    layout->setContentsMargins(0, 0, 9, 0); // only right margin (11 is default margin on most platforms)

    lineEditFilter->setFixedWidth(200);
    layout->addWidget(lineEditFilter);

    menuBar()->setCornerWidget(w, Qt::TopRightCorner);

    const QIcon aboutIcon = QIcon::fromTheme("help-about", QIcon(":/resources/help-about.png"));
    const QIcon helpIcon = QIcon::fromTheme("help-contents", QIcon(":/resources/help-contents.png"));
    helpMenu->addAction(helpIcon, tr("&Help"), this, &MainWindow::showHelp);
    helpMenu->addAction(aboutIcon, tr("&About"), this, &MainWindow::showAboutDialog);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    // On close, save results and auto archive done items
    refreshTasksFromFile(NO_READ_TASKS, WRITE_TASKS);
}

void MainWindow::showProjects(const QModelIndex &index)
{
    // Idea: To implement, add another QSortFilterProxy and chain the proxies
    // The filter in the menu bar acts on the last proxy and the listViewProjects
    // acts on the first proxy.
    QString selectedProject = index.data().toString();
    if (selectedProject == "Clear") {
        selectedProject = "";
    }
    lineEditFilter->setText(selectedProject);
}

void MainWindow::filterTasks(const QString &text)
{
    proxyTaskModel->setFilterRegularExpression(QRegularExpression(text, QRegularExpression::NoPatternOption));
    proxyTaskModel->setFilterKeyColumn(0);
}

void MainWindow::enterTask(const QString &text)
{
    bool enable = !text.trimmed().isEmpty();
    buttonAddTask->setEnabled(enable);
}

void MainWindow::newTaskEntered()
{
    addTask();
}

void MainWindow::addButtonClicked(bool checked)
{
    Q_UNUSED(checked);
    addTask();
}

void MainWindow::addTask()
{
    QString taskDescription = lineEditNewTask->text().trimmed();
    if (!taskDescription.isEmpty()) {
        taskModel->addTask(taskDescription);
    }

    lineEditNewTask->setText("");
}

void MainWindow::showHelp()
{
    QString description;

    description += "See <a href='https://github.com/todotxt/todo.txt'>todo.txt format</a> "
                   "for more information<br />and a primer on todo.txt.";

    QMessageBox help(QMessageBox::Information, tr("Suru"), description, QMessageBox::Close);
    help.exec();
}

void MainWindow::showAboutDialog()
{
    QString description;

    description += "Suru is a lean GUI application to work with "
                   "<a href='http://www.todotxt.com'>todo.txt</a> files.<br/>"
                   "<br/>"
                   "Copyright &copy; 2016-2025 Bart Clephas<br/>"
                   "Todo.txt, Copyright &copy; 2006-2016 Gina Trapani<br/>";

    QMessageBox::about(this, tr("Suru"), description);
}

void MainWindow::showPreferencesDialog()
{
    int dialogCode = preferencesDialog->exec();
    if (dialogCode == QDialog::Accepted) {
        QString folder = settings.value(PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING).toString();
        if (folder != previousFolder) {
            // Take easy way out. Warn user that unsaved data is lost.
            int ret = QMessageBox::warning(this, tr("Change todo.txt location?"),
                           tr("Switching the todo.txt folder location will result in loss of unsaved tasks.\n"
                              "If you want to retain this data, please cancel this operation and close the Suru first.\n"
                              "Do you want to continue?"),
                           QMessageBox::Yes | QMessageBox::No,
                           QMessageBox::No);

            if (ret == QMessageBox::No) {
                settings.setValue(PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING, previousFolder);
            }
        }

        settings.sync();

        updateUI();
    } else {
        // HACK: quick fix to restore the config (only for the TodoFilesFolder)
        settings.setValue(PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING, previousFolder);
    }
    // I really wish there was some kind of atomic update, like an accept or decline of the settings or an undo.
}

void MainWindow::refreshTasks()
{
    qDebug() << "Refresh task list";
    refreshTasksFromFile(READ_TASKS, WRITE_TASKS);
}

void MainWindow::refreshTasksFromFile(bool loadFromFile, bool saveToFile)
{
    bool result = false;

    QString folder = settings.value(PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING).toString();

    if (saveToFile == WRITE_TASKS) {
        qDebug() << "Saving to folder" << folder;
        result = taskModel->saveToDisk(folder);
        if (!result) {
            QMessageBox::critical(this,
                                  tr("Error saving todo.txt file"),
                                  tr("Could not save the todo.txt or done.txt file.\n"
                                     "Please ensure you have the required permissions.\n"
                                     "No tasks will be saved!\n\n"
                                     "File: %1/todo.txt\n"
                                     "File: %1/done.txt").arg(folder),
                                  QMessageBox::Ok);
        }
    }

    if (loadFromFile == READ_TASKS) {
        qDebug() << "Loading from folder" << folder;
        result = taskModel->loadFromDisk(folder);
        if (!result) {
            QMessageBox::critical(this,
                                  tr("Error loading todo.txt file"),
                                  tr("Could not read the todo.txt file.\n"
                                     "Please ensure you have the required permissions and the file is valid.\n"
                                     "No tasks will be displayed.\n\n"
                                     "File: %1/todo.txt").arg(folder),
                                  QMessageBox::Ok);
        }
    }
}

void MainWindow::updateUI()
{
    // reinitialize modules with this new data (if needed)
    QString folder = settings.value(PreferencesDialog::TODO_FILES_FOLDER_SETTINGS_STRING).toString();
    if (folder != previousFolder) {
        refreshTasksFromFile(READ_TASKS, NO_WRITE_TASKS);

        previousFolder = folder;
    }

    bool showProjectsPanel = settings.value(PreferencesDialog::SHOW_PROJECTS_PANEL_SETTINGS_STRING).toBool();
    qDebug() << "Show projects panel" << showProjectsPanel;
    if (showProjectsPanel) {
        listViewProjects->show();
    } else {
        listViewProjects->hide();
    }
}

