/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "SortTaskProxyModel.h"
#include "TaskModel.h"
#include "TodoTask.h"

#include <QDebug>

SortTaskProxyModel::SortTaskProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

bool SortTaskProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData  = sourceModel()->data(left, sortRole());
    QVariant rightData = sourceModel()->data(right, sortRole());

    TaskModel::TaskAdapter leftTask  = leftData.value<TaskModel::TaskAdapter>();
    TaskModel::TaskAdapter rightTask = rightData.value<TaskModel::TaskAdapter>();

    // If both complete, sort according task id
    if (leftTask.task->complete && rightTask.task->complete) {
        return leftTask.task_id < rightTask.task_id;
    }
    // If either one is completed and the other not, completed item comes last
    if (leftTask.task->complete || rightTask.task->complete) {
        if (leftTask.task->complete) {
            return false;
        }
        return true;
    }

    // If no priority, sort according task id
    if (leftTask.task->priority == '\0' && rightTask.task->priority == '\0') {
        return leftTask.task_id < rightTask.task_id;
    }

    // If both have an equal priority, sort according task id
    if (leftTask.task->priority && rightTask.task->priority &&
        leftTask.task->priority == rightTask.task->priority) {
        return leftTask.task_id < rightTask.task_id;
    }
    // If both have (a non-equal) priority, sort according priority
    if (leftTask.task->priority && rightTask.task->priority) {
        return leftTask.task->priority < rightTask.task->priority;
    }
    // If either one has priority, priority task is first
    if (leftTask.task->priority || rightTask.task->priority) {
        if (leftTask.task->priority) {
            return true;
        }
        return false;
    }

    qDebug() << "Could not decide on sort mechanism, fall back to default behavior";
    return QSortFilterProxyModel::lessThan(left, right);
}

