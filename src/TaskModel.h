/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#ifndef TODOTXT_TASK_MODEL
#define TODOTXT_TASK_MODEL

#include <QAbstractItemModel>
#include <QMetaType>
#include <QVector>

#include <memory>

class TodoTask;

typedef std::shared_ptr<TodoTask> TodoTask_ptr;

/* TaskModel is an adapter (and manager) between a task
 * and the UI
 */
class TaskModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    // A model specific adapter for the model to
    // allow sorting.
    struct TaskAdapter
    {
        TaskAdapter();
        TaskAdapter(qsizetype task_id, TodoTask_ptr task);
        TaskAdapter(const TaskAdapter &);
        virtual ~TaskAdapter();

        qsizetype    task_id;
        TodoTask_ptr task;
    };

public:
    TaskModel(QObject *parent = NULL);
    virtual ~TaskModel();

    void addTask(const QString taskDescription);

    bool loadFromDisk(QString basePath);
    bool saveToDisk(QString basePath);

    // QAbstractItemModel interface
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &child) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    bool hasChildren(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

signals:
    void projectsChanged(QVector<QString>) const;

private:
    QVector<QString> projects() const;
    void notifyListeners() const;
    void debugModel(const QVector<TaskAdapter> &model, const QString context = "");

private:
    QVector<TaskAdapter> tasks;
};

Q_DECLARE_METATYPE(TaskModel::TaskAdapter);

#endif /* TODOTXT_TASK_MODEL */

