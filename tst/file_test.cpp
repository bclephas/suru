/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "TodoFile.h"
#include "TodoTask.h"

#include <ios>

/* Craft a specific test version of File to expose protected interfaces */
class TodoFileForTest : public TodoFile
{
public:
    using TodoFile::load;
    using TodoFile::writeTasks;
};

TEST(TodoFileTest, ConstructionDefaultsToEmptyTaskList)
{
    // Arrange
    TodoFile f;

    // Assert
    ASSERT_EQ(0, f.tasks().size());
}

TEST(TodoFileTest, SetTasksCanBeRetrieved)
{
    // Arrange
    TodoFile f;
    std::vector<TodoTask_ptr> tasks;
    tasks.push_back(TodoTask_ptr(new TodoTask("foo")));
    f.setTasks(tasks);

    // Act
    std::vector<TodoTask_ptr> task_copy = f.tasks();

    // Assert
    ASSERT_EQ(1, task_copy.size());
    ASSERT_STREQ("foo", task_copy[0]->raw_line.c_str());
}

TEST(TodoFileTest, LoadingTasks_FileIsEmpty)
{
    // Arrange
    TodoFileForTest f;
    std::istringstream iss("");

    // Act
    f.load(iss);

    // Assert
    std::vector<TodoTask_ptr> task_copy = f.tasks();
    ASSERT_EQ(0, task_copy.size());
}

TEST(TodoFileTest, LoadingTasks_FileHasTasks_TasksAreLoadedCorrectly)
{
    // Arrange
    TodoFileForTest f;
    std::istringstream iss("task one\n\ntask two with +project and @context");

    // Act
    f.load(iss);

    // Assert
    std::vector<TodoTask_ptr> task_copy = f.tasks();
    ASSERT_EQ(2, task_copy.size());
    ASSERT_STREQ("task one", task_copy[0]->raw_line.c_str());
    ASSERT_STREQ("task two with +project and @context", task_copy[1]->raw_line.c_str());
}

TEST(TodoFileTest, SaveWritesTheCorrectData)
{
    // Arrange
    TodoFileForTest f;
    std::vector<TodoTask_ptr> tasks;
    tasks.push_back(std::shared_ptr<TodoTask>(new TodoTask("foo")));
    tasks.push_back(std::shared_ptr<TodoTask>(new TodoTask("bar")));
    tasks.push_back(std::shared_ptr<TodoTask>(new TodoTask("x foo")));
    tasks.push_back(std::shared_ptr<TodoTask>(new TodoTask("x bar")));
    f.setTasks(tasks);

    std::ostringstream ossTodo;
    std::ostringstream ossDone;

    // Act
    f.writeTasks(ossTodo, TodoFile::WRITE_ONLY_INCOMPLETE_TASKS);
    f.writeTasks(ossDone, TodoFile::WRITE_ONLY_COMPLETE_TASKS);

    // Assert
    ASSERT_STREQ("foo\nbar\n", ossTodo.str().c_str());
    ASSERT_STREQ("x foo\nx bar\n", ossDone.str().c_str());
}

TEST(TodoFileTest, Loading_FailsForEmptyFile_NothingLoaded)
{
    // Arrange
    TodoFileForTest f;
    std::istringstream iss("");
    iss.setstate(std::ios::failbit);

    // Act
    bool result = f.load(iss);

    // Assert
    ASSERT_FALSE(result) << "Expected the load to fail on a set ios::failbit";
}

TEST(TodoFileTest, Saving_Fails_NothingWritten)
{
    // Arrange
    TodoFileForTest f;
    std::ostringstream oss;
    oss.setstate(std::ios::failbit);

    // Act
    bool result = f.writeTasks(oss, TodoFile::WRITE_ONLY_INCOMPLETE_TASKS);

    // Assert
    ASSERT_FALSE(result) << "Expected the save to fail on a set ios::failbit";
}

