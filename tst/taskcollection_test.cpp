/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "TodoTaskCollection.h"
#include "TodoTask.h"

TEST(TodoTaskCollectionTest, NoTasksMeansNoProjectsOrContexts)
{
    TodoTaskCollection collection;
    ASSERT_EQ(0, collection.getProjects().size());
    ASSERT_EQ(0, collection.getContexts().size());
}

TEST(TodoTaskCollectionTest, AllProjectsFromOneTaskFound)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one +project1 +project2"));
    collection.items.push_back(newTask);

    std::vector<std::string> projects = collection.getProjects();
    ASSERT_EQ(2, projects.size());
    ASSERT_EQ("project1", projects[0]);
    ASSERT_EQ("project2", projects[1]);
}

TEST(TodoTaskCollectionTest, AllProjectsFromMultipleTasksFound)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one +project1 +project2"));
    collection.items.push_back(newTask);
    TodoTask_ptr newTask2(new TodoTask("two +project3 +project4"));
    collection.items.push_back(newTask2);

    std::vector<std::string> projects = collection.getProjects();
    ASSERT_EQ(4, projects.size());
    ASSERT_EQ("project1", projects[0]);
    ASSERT_EQ("project2", projects[1]);
    ASSERT_EQ("project3", projects[2]);
    ASSERT_EQ("project4", projects[3]);
}

TEST(TodoTaskCollectionTest, DuplicateProjectsAreRemoved)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one +project1 +project2"));
    collection.items.push_back(newTask);
    TodoTask_ptr newTask2(new TodoTask("two +project1 +project2"));
    collection.items.push_back(newTask2);
    TodoTask_ptr newTask3(new TodoTask("three +project1 +project1"));
    collection.items.push_back(newTask3);

    std::vector<std::string> contexts = collection.getProjects();
    ASSERT_EQ(2, contexts.size());
    ASSERT_EQ("project1", contexts[0]);
    ASSERT_EQ("project2", contexts[1]);
}

TEST(TodoTaskCollectionTest, AllContextsFromOneTaskFound)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one @context1 @context2"));
    collection.items.push_back(newTask);

    std::vector<std::string> contexts = collection.getContexts();
    ASSERT_EQ(2, contexts.size());
    ASSERT_EQ("context1", contexts[0]);
    ASSERT_EQ("context2", contexts[1]);
}

TEST(TodoTaskCollectionTest, AllContextsFromMultipleTasksFound)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one @context1 @context2"));
    collection.items.push_back(newTask);
    TodoTask_ptr newTask2(new TodoTask("two @context3 @context4"));
    collection.items.push_back(newTask2);

    std::vector<std::string> contexts = collection.getContexts();
    ASSERT_EQ(4, contexts.size());
    ASSERT_EQ("context1", contexts[0]);
    ASSERT_EQ("context2", contexts[1]);
    ASSERT_EQ("context3", contexts[2]);
    ASSERT_EQ("context4", contexts[3]);
}

TEST(TodoTaskCollectionTest, DuplicateContextsAreRemoved)
{
    TodoTaskCollection collection;
    TodoTask_ptr newTask(new TodoTask("one @context1 @context2"));
    collection.items.push_back(newTask);
    TodoTask_ptr newTask2(new TodoTask("two @context1 @context2"));
    collection.items.push_back(newTask2);
    TodoTask_ptr newTask3(new TodoTask("three @context1 @context1"));
    collection.items.push_back(newTask3);

    std::vector<std::string> contexts = collection.getContexts();
    ASSERT_EQ(2, contexts.size());
    ASSERT_EQ("context1", contexts[0]);
    ASSERT_EQ("context2", contexts[1]);
}

