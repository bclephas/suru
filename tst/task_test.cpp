/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "TodoTask.h"

TEST(TodoTaskTest, DestructionOfTaskOnStackShouldSucceed)
{
    TodoTask empty("");
}

TEST(TodoTaskTest, DestructionOfTaskOnHeapShouldSucceed)
{
    TodoTask *empty2 = new TodoTask("");
    delete empty2;
}

TEST(TodoTaskTest, ConstructionOfEmptyTasks)
{
    // Arrange
    TodoTask empty("");
    TodoTask empty2("   ");

    // Assert
    ASSERT_STREQ("", empty.raw_line.c_str());
    ASSERT_STREQ("", empty2.raw_line.c_str());
}

TEST(TodoTaskTest, ConstructionSetsAppropriateFields)
{
    // Arrange
    TodoTask task("(A) some words +home @phone +work @mail");

    // Assert
    ASSERT_EQ('A', task.priority);
    ASSERT_FALSE(task.complete);
    ASSERT_EQ(2, task.projects.size());
    ASSERT_STREQ("home", task.projects[0].c_str());
    ASSERT_STREQ("work", task.projects[1].c_str());
    ASSERT_EQ(2, task.contexts.size());
    ASSERT_STREQ("phone", task.contexts[0].c_str());
    ASSERT_STREQ("mail", task.contexts[1].c_str());
    ASSERT_STREQ("some words +home @phone +work @mail", task.description.c_str());
    ASSERT_STREQ("(A) some words +home @phone +work @mail", task.raw_line.c_str());
}

TEST(TodoTaskTest, CanParseCreationDate)
{
    TodoTask datedTask("2000-01-01 some text");

    struct tm creation_date;
    creation_date.tm_year = 100; /* 1900 + x */
    creation_date.tm_mon  = 0;
    creation_date.tm_mday = 1;
    ASSERT_EQ(creation_date.tm_year, datedTask.creation_date.tm_year);
    ASSERT_EQ(creation_date.tm_mon, datedTask.creation_date.tm_mon);
    ASSERT_EQ(creation_date.tm_mday, datedTask.creation_date.tm_mday);
}

TEST(TodoTaskTest, CanParseCompletionDate)
{
    TodoTask datedTask("x 2000-01-01 2001-02-03 some text");

    struct tm completion_date;
    completion_date.tm_year = 100; /* 1900 + x */
    completion_date.tm_mon  = 0;
    completion_date.tm_mday = 1;
    ASSERT_EQ(completion_date.tm_year, datedTask.completion_date.tm_year);
    ASSERT_EQ(completion_date.tm_mon, datedTask.completion_date.tm_mon);
    ASSERT_EQ(completion_date.tm_mday, datedTask.completion_date.tm_mday);

    struct tm creation_date;
    creation_date.tm_year = 101; /* 1900 + x */
    creation_date.tm_mon  = 1;
    creation_date.tm_mday = 3;
    ASSERT_EQ(creation_date.tm_year, datedTask.creation_date.tm_year);
    ASSERT_EQ(creation_date.tm_mon, datedTask.creation_date.tm_mon);
    ASSERT_EQ(creation_date.tm_mday, datedTask.creation_date.tm_mday);
}

// TODO: create helper function(s) to verify task properties

TEST(TodoTaskTest, CanConstructVariousTasks)
{
    // Arrange
    TodoTask one("(A) some words +home @phone +work @mail");
    TodoTask two("x other sentence");
    TodoTask three("xavier is not complete");
    TodoTask four("X does not mean completion");
    TodoTask five("Words +home intermixed @phone with +work contexts @mail and projects");
    TodoTask six("2000-01-01 Words +home intermixed @phone with +work contexts @mail and projects");
    TodoTask seven("x 2000-01-01 only completion date");
    TodoTask eight("x 2000-01-01 2000-01-02 both complete and creation date");
    TodoTask empty("");
    TodoTask empty2("   ");
}

