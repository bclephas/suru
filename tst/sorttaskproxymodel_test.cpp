/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "SortTaskProxyModel.h"
#include "TaskModel.h"

/* Helper function that converts to a QString and compares for string equality */
#define ASSERT_QSTREQ(val1, val2) ASSERT_EQ(QString(val1).toStdString(), QString(val2).toStdString())

class SortTaskProxyModelTest : public ::testing::Test
{
public:
    virtual void SetUp()
    {
        model = new TaskModel();
    }

    virtual void TearDown()
    {
        delete model;
    }

    TaskModel *model;
};

TEST_F(SortTaskProxyModelTest, VerifyBasicConstruction)
{
    model->addTask(QString("simple task"));

    // Create model and wrap proxy, verify
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);

    ASSERT_QSTREQ("simple task", proxy.data(proxy.index(0, 0)).value<QString>());
}

TEST_F(SortTaskProxyModelTest, VerifyBasicTaskNoSort)
{
    model->addTask(QString("first task"));
    model->addTask(QString("second task"));

    // Create model and wrap proxy, verify
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.sort(0);

    ASSERT_QSTREQ("first task", proxy.data(proxy.index(0, 0)).value<QString>());
    ASSERT_QSTREQ("second task", proxy.data(proxy.index(1, 0)).value<QString>());
    ASSERT_EQ(0, proxy.data(proxy.index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(1, proxy.data(proxy.index(1, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
}

TEST_F(SortTaskProxyModelTest, VerifySortIsCorrect)
{
    model->addTask(QString("first task"));
    model->addTask(QString("second task"));
    model->addTask(QString("third task +Tag"));
    model->addTask(QString("third task +TAG"));
    model->addTask(QString("x finished task"));
    model->addTask(QString("(A) highest priority task"));
    model->addTask(QString("(B) priority task"));
    model->addTask(QString("x another task"));
    model->addTask(QString("aa task"));

    // Create model and wrap proxy
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.sort(0);

    // Verify that tasks shown are ordered on priority and completion
    ASSERT_QSTREQ("(A) highest priority task", proxy.data(proxy.index(0, 0)).value<QString>());
    ASSERT_QSTREQ("(B) priority task", proxy.data(proxy.index(1, 0)).value<QString>());
    ASSERT_QSTREQ("first task", proxy.data(proxy.index(2, 0)).value<QString>());
    ASSERT_QSTREQ("second task", proxy.data(proxy.index(3, 0)).value<QString>());
    ASSERT_QSTREQ("third task +Tag", proxy.data(proxy.index(4, 0)).value<QString>());
    ASSERT_QSTREQ("third task +TAG", proxy.data(proxy.index(5, 0)).value<QString>());
    ASSERT_QSTREQ("aa task", proxy.data(proxy.index(6, 0)).value<QString>());
    ASSERT_QSTREQ("x finished task", proxy.data(proxy.index(7, 0)).value<QString>());
    ASSERT_QSTREQ("x another task", proxy.data(proxy.index(8, 0)).value<QString>());

    // Verify that task indexes are as entered
    ASSERT_EQ(5, proxy.data(proxy.index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(6, proxy.data(proxy.index(1, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(0, proxy.data(proxy.index(2, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(1, proxy.data(proxy.index(3, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(2, proxy.data(proxy.index(4, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(3, proxy.data(proxy.index(5, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(8, proxy.data(proxy.index(6, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(4, proxy.data(proxy.index(7, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(7, proxy.data(proxy.index(8, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
}

TEST_F(SortTaskProxyModelTest, VerifySortForEqualPriorityIsCorrectAfterTaskModification)
{
    model->addTask(QString("prio 1"));
    model->addTask(QString("prio 2"));

    // Create model and wrap proxy
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.sort(0);

    // Modify in reverse order
    model->setData(model->index(1, 0), QVariant("(A) prio 2"));//, Qt::UserRole);
    model->setData(model->index(0, 0), QVariant("(A) prio 1"));//, Qt::UserRole);

    // Verify that tasks shown are ordered on priority and completion
    ASSERT_QSTREQ("(A) prio 1", proxy.data(proxy.index(0, 0)).value<QString>());
    ASSERT_QSTREQ("(A) prio 2", proxy.data(proxy.index(1, 0)).value<QString>());

    // Verify that task indexes are as entered
    ASSERT_EQ(0, proxy.data(proxy.index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
    ASSERT_EQ(1, proxy.data(proxy.index(1, 0), Qt::UserRole).value<TaskModel::TaskAdapter>().task_id);
}

TEST_F(SortTaskProxyModelTest, VerifySimpleSearchReturningAppropriateResults)
{
    model->addTask(QString("prio 1"));
    model->addTask(QString("prio 2"));
    model->addTask(QString("prio 3"));

    // Create model and wrap proxy
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.setFilterRegularExpression(QRegularExpression("3", QRegularExpression::NoPatternOption));

    // Verify that correct tasks are shown
    ASSERT_EQ(proxy.rowCount(), 1);
    ASSERT_QSTREQ("prio 3", proxy.data(proxy.index(0, 0)).toString());
}

TEST_F(SortTaskProxyModelTest, VerifyCaseSensitiveSearchReturningAppropriateResults)
{
    model->addTask(QString("prio a"));
    model->addTask(QString("prio A"));

    // Create model and wrap proxy
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.setFilterRegularExpression(QRegularExpression("A", QRegularExpression::NoPatternOption));

    // Verify that correct tasks are shown
    ASSERT_EQ(proxy.rowCount(), 1);
    ASSERT_QSTREQ("prio A", proxy.data(proxy.index(0, 0)).toString());
}


TEST_F(SortTaskProxyModelTest, VerifyComplexSearchReturningAppropriateResults)
{
    model->addTask(QString("prio 1"));
    model->addTask(QString("prio 20"));
    model->addTask(QString("prio 300"));

    // Create model and wrap proxy
    SortTaskProxyModel proxy;
    proxy.setSourceModel(model);
    proxy.setDynamicSortFilter(true);
    proxy.setSortRole(Qt::UserRole);
    proxy.setFilterRegularExpression(QRegularExpression("prio \\b\\d{2}\\b", QRegularExpression::NoPatternOption));

    // Verify that correct tasks are shown
    ASSERT_EQ(proxy.rowCount(), 1);
    ASSERT_QSTREQ("prio 20", proxy.data(proxy.index(0, 0)).toString());
}

