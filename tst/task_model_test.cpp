/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "TaskModel.h"
#include "TodoTask.h"

#include <modeltest.h>

/* Helper function that converts to a QString and compares for string equality */
#define ASSERT_QSTREQ(val1, val2) ASSERT_EQ(QString(val1).toStdString(), QString(val2).toStdString())

class TaskModelTest : public ::testing::Test
{
public:
    virtual void SetUp()
    {
        model = new TaskModel();
        modelTest = new ModelTest(model);
    }
    
    virtual void TearDown()
    {
        // Destroy model first to detect issues when destructing
        delete model;
        delete modelTest;
    }

    ModelTest *modelTest;
    TaskModel *model;
};

TEST_F(TaskModelTest, VerifyBasicConstruction)
{
    ASSERT_EQ(QModelIndex(), model->index(0, 0));
    ASSERT_EQ(QModelIndex(), model->parent(QModelIndex()));
    ASSERT_EQ(0, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_FALSE(model->hasChildren());

    Qt::ItemFlags setFlags = model->flags(QModelIndex());
    ASSERT_TRUE(Qt::ItemIsSelectable & setFlags);
    ASSERT_TRUE(Qt::ItemIsEditable & setFlags);
    ASSERT_TRUE(Qt::ItemIsEnabled & setFlags);

    ASSERT_EQ(QVariant(), model->data(QModelIndex()));
}

TEST_F(TaskModelTest, AddingASingleTaskAllowsRetrieval)
{
    model->addTask(QString("simple task"));
    ASSERT_EQ(1, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("simple task", model->data(model->index(0, 0)).value<QString>());
}

TEST_F(TaskModelTest, AddingMultipleTasksAllowsForCorrectRetrieval)
{
    model->addTask(QString("first task"));
    model->addTask(QString("second task"));
    ASSERT_EQ(2, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("first task", model->data(model->index(0, 0)).value<QString>());
    ASSERT_QSTREQ("second task", model->data(model->index(1, 0)).value<QString>());
}

TEST_F(TaskModelTest, CanRetrieveWholeTask)
{
    model->addTask(QString("first task"));
    TaskModel::TaskAdapter task = model->data(model->index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(0, task.task_id);
    ASSERT_EQ("first task", task.task->raw_line);
    ASSERT_FALSE(task.task->complete);
}

TEST_F(TaskModelTest, DataCanBeModified)
{
    model->addTask(QString("first task"));
    model->setData(model->index(0, 0), QVariant("x other task"));
    TaskModel::TaskAdapter task = model->data(model->index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(0, task.task_id);
    ASSERT_EQ("x other task", task.task->raw_line);
    ASSERT_TRUE(task.task->complete);
}

TEST_F(TaskModelTest, OnlyModifiedItemIsUpdatedOthersRemainUnmodified)
{
    model->addTask(QString("first task"));
    model->addTask(QString("second task"));
    model->setData(model->index(1, 0), QVariant("x other task"));

    TaskModel::TaskAdapter task;
    task = model->data(model->index(1, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(1, task.task_id);
    ASSERT_EQ("x other task", task.task->raw_line);
    ASSERT_TRUE(task.task->complete);

    task = model->data(model->index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(0, task.task_id);
    ASSERT_EQ("first task", task.task->raw_line);
}

TEST_F(TaskModelTest, InsertingItemsWillNotReorderData)
{
    model->addTask(QString("x first task"));
    model->addTask(QString("second task"));

    TaskModel::TaskAdapter task;
    task = model->data(model->index(0, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(0, task.task_id);
    ASSERT_EQ("x first task", task.task->raw_line);
    ASSERT_TRUE(task.task->complete);
    
    task = model->data(model->index(1, 0), Qt::UserRole).value<TaskModel::TaskAdapter>();
    ASSERT_EQ(1, task.task_id);
    ASSERT_EQ("second task", task.task->raw_line);
    ASSERT_FALSE(task.task->complete);
}

