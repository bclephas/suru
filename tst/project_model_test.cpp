/*
    Suru - a lean GUI application to work with todo.txt
    Copyright (C) 2016-2025  Bart Clephas <bclephas@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
#include "gtest/gtest.h"
#include "ProjectModel.h"
#include "TodoTask.h"

#include <modeltest.h>

/* Helper function that converts to a QString and compares for string equality */
#define ASSERT_QSTREQ(val1, val2) ASSERT_EQ(QString(val1).toStdString(), QString(val2).toStdString())

class ProjectModelTest : public ::testing::Test
{
public:
    virtual void SetUp()
    {
        model = new ProjectModel();
        modelTest = new ModelTest(model);
    }

    virtual void TearDown()
    {
        // Destroy model first to detect issues when destructing
        delete model;
        delete modelTest;
    }

    ModelTest *modelTest;
    ProjectModel *model;
};

TEST_F(ProjectModelTest, VerifyBasicConstruction)
{
    ASSERT_EQ(QModelIndex(), model->index(0, 0));
    ASSERT_EQ(QModelIndex(), model->parent(QModelIndex()));
    ASSERT_EQ(0, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_FALSE(model->hasChildren());

    Qt::ItemFlags setFlags = model->flags(QModelIndex());
    ASSERT_TRUE(Qt::ItemIsSelectable & setFlags);
    ASSERT_TRUE(Qt::ItemIsEnabled & setFlags);
    ASSERT_FALSE(Qt::ItemIsEditable & setFlags);

    ASSERT_EQ(QVariant(), model->data(QModelIndex()));
}

TEST_F(ProjectModelTest, AddingASingleTaskAllowsRetrieval)
{
    QVector<QString> projects; projects << "+Project 1";
    model->setProjects(projects);
    ASSERT_EQ(2, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("Clear", model->data(model->index(0, 0)).value<QString>());
    ASSERT_QSTREQ("+Project 1", model->data(model->index(1, 0)).value<QString>());
}

TEST_F(ProjectModelTest, AddingMultipleTasksAllowsForCorrectRetrieval)
{
    QVector<QString> projects; projects << "+Project 1" << "+Project 2";
    model->setProjects(projects);
    ASSERT_EQ(3, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("+Project 1", model->data(model->index(1, 0)).value<QString>());
    ASSERT_QSTREQ("+Project 2", model->data(model->index(2, 0)).value<QString>());
}

TEST_F(ProjectModelTest, InsertingItemsWillNotReorderData)
{
    QVector<QString> projects; projects << "+Project 1" << "+Project 2";
    model->setProjects(projects);

    ASSERT_EQ(3, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("+Project 1", model->data(model->index(1, 0)).value<QString>());
    ASSERT_QSTREQ("+Project 2", model->data(model->index(2, 0)).value<QString>());

    projects << "+Project 3";
    model->setProjects(projects);

    ASSERT_EQ(4, model->rowCount());
    ASSERT_EQ(1, model->columnCount());
    ASSERT_QSTREQ("+Project 1", model->data(model->index(1, 0)).value<QString>());
    ASSERT_QSTREQ("+Project 3", model->data(model->index(3, 0)).value<QString>());
}

